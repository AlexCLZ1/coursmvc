<?php

include '../Model/bdd.php';

$bdd = New bdd();
if(!isset($_GET['article'])){

    $articles = $bdd->fetchArticles();

   include '../View/article.php';

}elseif($_GET['article'] == 'getOneById'){

    $id = $_GET['id'];

    $article = $bdd->fetchArticleById($id);

    include '../View/articleDetail.php';

}   elseif ($_GET['article'] == 'delete'){
    $id = $_GET['id'];
    $bdd->deleteArticle($id);;
    header('Location: ../Controller/article.php');

} elseif ($_GET['article'] == 'add') {
    $libelle = $_POST['libelle'];
    $prixht = $_POST['prixht'];
    $prix = $_POST['prix'];
    $categorie = $_POST['categorie'];
    $bdd->addArticle($libelle, $prixht, $prix, $categorie);
    header('Location: ../Controller/article.php');

} elseif ($_GET['article'] == 'beforeAdd') {
    $categories = $bdd->fetchCategorie();
    include ('..\View\newArticle.php');}